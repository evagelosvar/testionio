@ΑΙΩΝ Δ'. - ΒΑΣΙΛΕΙΟΣ ΚΑΙΣΑΡΙΑΣ Ο ΜΕΓΑΣ - ΤΟΜΟΣ 29 {p 70}

Ομολογία της καθολικής πίστεως, ήν έδωκε προς Ιούλιον Πάπαν Ρώμης-Τύπος δεύτερος του Συμβόλου. 1585. 
Έκθεσις ομολογίας της καθολικής πίστεως-
Τύπος τρίτος του συμβόλου. 1585. 
Τύπος τέταρτος του συμβόλου 1588. Σύμβολον Αθανασίου εν τη αρχαία Γαλλική γλώσση. 1592. 
Αρχαία ερμηνεία του Συμβόλου· λατιν. 1596. 
Τεμάχιον εκ του περί τρίαδος Αθανασίου. 1604. 
Ερμηνεία του περί του Πάσχα λόγου Αθανασίου· λατινιστί. 1605.
Λεξικόν εις άγιον Αθανάσιον. 1609.
Ονομαστικόν λέξεων αήθων καί δυσλήπτων εν τω κη΄.τόμω αναγινωσκομένων· ελληνολατινιστί. 1617.
Πίναξ αναλυτικός· λατινιστί. 1623.
Πίναξ των περιεχομένων εν τω εικοστώ ογδόω τόμω· λατινιστί. 1633.
ΠΡΟΣΘΗΚΑΙ
Πίστις των αγίων τριακοσίων δέκα καί οκτώ αγίων θεοφόρον Πατέρων των εν Νικαία· και διδασκαλία πάνυ θαυμαστή καί σωτήριος περί της αγίας Τριάδος. 1637.
Τάξις των συγγραμμάτων Αθανασίου εν ταις προγενεστέραις εκδόσεσι μετά παραβολης πρός τήν παρούσαν· λατινιστί. 1645.
Τέλος των εν τω εικοστώ ογδόω τόμω.

ΒΑΣΙΛΕΙΟΣ ΚΑΙΣΑΡΕΙΑΣ Ο ΜΕΓΑΣ
(329-379 Μ.Χ.)
ΤΟΥ ΕΝ ΑΓΙΟΙΣ ΠΑΤΡΟΣ ΗΜΩΝ ΒΑΣΙΛΕΙΟΥ ΤΟΥ ΜΕΓΑΛΟΥ ΤΑ ΕΥΡΙΣΚΟΜΕΝΑ ΠΑΝΤΑ (α)

ΤΟΜΟΣ 29

ΠΕΡΙΕΧΟΜΕΝΑ

ΠΡΟΛΕΓΟΜΕΝΑ
Βίος του αγίου Βασιλείου· λατινιστί. V.
Προοίμιον· λατινιστί. CLXXVII.
Σχόλια εκ της βιβλιοθήκης Φαβρικίου· λατινιστί. CCXLI.
Περί των παλαιών εκδόσεων του αγίου Βασιλείου προλεγόμενα άξια λόγου· λατινιστί. CCLXXV.
Συλλογή έκ των πράξεων τού αγίου Βασιλείου· λατινιστί. CCLXXXV.
Απόκρυφα περί του βίου του αγίου Βασιλείου· λατινιστί. CCXCIV.
Προλεγόμενα είς τήν ακολουθίαν τών τριών Ιεραρχών· λατινιστί. CCCXVI.
Ακολουθία των τριών Ιεραρχών. CCCXXVI.
ΣΥΓΓΡΑΜΜΑΤΑ
Ομιλίαι είς την Εξαήμερον 
Θ΄. 4-208.
Α΄. Έν αρχή εποίησεν ο Θεός τόν ουρανόν καί την γήν. 4.
Β΄. Ή δέ γή ήν αόρατος καιί ακατασκεύαστος. 28.
Γ΄. Περί τού στερεώματος. 52.
Δ΄. Περί συναγωγής τών υδάτων. 77.
Ε΄. Περί βλαστήσεως γής. 93.
ς΄. Περί γενέσως φωστήρων. 117.
Ζ΄. Περί ερπετών. 148.
Η΄. Περί πτηνών καί ενύδρων. 164.
Θ΄. Περί χερσαίων. 188.
(α) Είς τόμους τέσσαρας (29-32).
