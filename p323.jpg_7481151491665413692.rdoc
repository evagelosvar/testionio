ΑΙΩΝ Ε'.  ΕΠΙΣΤΟΛΑΙ  ΤΟΜΟΣ 77. {269 p}

KE'. Τις η θλασθείσα κεφαλή. 1128.
Κ#'. Την απειρόγαμον άφθορον ονομάζειν οίδεν η των ανθρώπων 
συνήθεια. 1128.
ΚZ'. Τον μεταξύ του γναού και του θυσιαστηρίου φονευθέντα Ζαχαρίαν, ουκ εύκαιρον ην εις µαρτυρίαν της αφθόρου Μητρός παραστήσασθαι. 1129.
ΚΗ'. Διά τι δοξάζει η των αγγέλων φωνή τον εν τοις υψίστοις θεωρουµένην θεότητα, το, «Δόξα εν υψίστοις», λέχουσα, ότι φησί «και
επί γης ειρήνη». 1132.

Προς τον ευσεθέστατον βασιλέα Θεοδόσιον προσφωνητικός. Περί της ορθής πίστεως της εις τον Κύριον ημών Ιησούν Χριστόν εν παραγράφοις ΜΕ'. 1133.
Λόγος προσφωνητικός ταίς ευσεθεστάταις βασιλίσσαις. 1201.
Λόγος προσφωνητικός ταίς ευσεθεστάταις βασιλίσσαις, περί της ορθής πίστεως. 1336.
Τεμάχια δογματικά" ελληνιστί και λατ. 1421.
Του αγίου Κυρίλλου, εκ του λόγου, ου η επιγραφή: «Απόδειξις διά των αρχίθεν γεγενηµένων θείων χρησμών τοις θεσπεσίοις πατριάρχαις περί της αμείψεως και µεταθέσεως των Ιουδαίων και των εθνών». 1421.
Κύριλλος ο σοφώτατος και της ᾽Αλεξανδρέων πόλεως επίσκοπος εν τω επιγεγραμμένω λόγω αυτού" «Προς τους τολμώντας λέγει, μη δείν υπέρ των εν πίστει κεκοιμημένων
προσφέρειν», ου η αρχή: «Οι της εαυτών διανοίας τον οφθαλμόν απευθύνοντες κτλ.» 1424.

*Γραφικαί διαφοραί εν τοις Κυρίλλου συγγράμμασιν. 1453.
*Πίναξ αναλυτικός' λατινιστί. 1464.
*Πίναξ των περιεχομένων εν τω εβδομηκοστω έκτω τόμω λατινιστί, 1479.

Τέλος των εν τω εβδομηκοστώ έκτω τόμω.


ΤΟΜΟΣ 77

ΠΕΡΙΕΧΟΜΕΝΑ

Επιστολαί Κυρίλλου Άλεξανδρείας ΠΗ'. 9--390.
Α΄. Κύριλλος πρεσθυτέροις και διακόνοις, πατράσι μοναχοίς και τοις συν υμίν τον µονήρη βίον ασκούσι, και εν πίστει Θεού ίδρυμένοις, αγαπητοίς και ποθεινοτάτοις, εν Κυρίω χαίρειν (περί της αγίας Παρθένου Θεο-
τόκου). 9.
Β'. Προς Νεστόριον Κωνσταντινουπόλεως" (περί
της αγίας παρθένου και Θεοτόκου). 40.
Γ'. Νεστορίου προς Κύριλλον. 44.
Δ'. Κυρίλλου προς Νεστόριον, (περί του Θεού
Λόγου). 44.
Ε'. Νεστορίου προς Κύριλλον, 49
#--Ζ'. Πρός Νεστόριον λατινιστί. 57.
Η'. Κυρίλλου προς τούς αθτόν αιτικιασαμένους, ότι σεσιώπηκεν, εξ ακοής μαθών έρπειν επί το χείρων την δυσσεβή του Νεστορίου διδασκαλίαν. 60.
Θ'. Κυρίλλου απάντησις εις την ανωτέρω. 61.
Ι'. Περί του Νεστορίου και των βλασφημίων αυτού προς τούς Κωσταντινουπόλεως κληρικούς στασιάζοντας. 64.
Προς Κελεστίνον τον Ρώμης λατινιστί. 69.
ΙΑ'. Προς Κελεστίνου τον Ρώμης. 80.
ΙΒ'. Κελεστίνου προς Κύριλλον. 89.
ΙΓ'. Κυρίλλου προς Ιωάννην Αντειοχείας. 93.
ΙΔ'. Κυρίλλου προς Ακάκιον Βερροίας. 97.
ΙΕ'. Ακακίου προς Κύριλλον. 100.
Ι#'. Κυρίλλου προς Ιεβενάλιον. 104.
ΙΖ'. Κυρίλλου προς Νεστόριον. 105.
ΙΗ'. Κυρίλλου προς τον κλήρον και λαόν Κωνσταντονουπόλεως. 124.
ΙΘ'. Κυρίλλου προς τούς εν Κωνασταντινουπόλει

