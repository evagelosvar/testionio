ΑΙΩΝ Δ'. - ΣΧΟΛΙΑΣΤΑΙ ΓΡΗΓΟΡΙΟΥ ΝΑΖΙΑΝΖΗΝΟΥ - ΤΟΜΟΣ 38
Λόγος δ'. καί ε'. εις εμαυτόν. 406.
Πρός δέ:
Περί τού πένθους Ιακώβ εις τόν Ιωσήφ. 406.
περί τού πένθους Ιωσήφ εις τόν πατέρα˙ αυτόθι.
περί τού πένθους Αβραάμ εις Σάρραν. 407.
περί τού πένθους Ιερεμίου τού προφήτου˙ αυτόθι.
περί τής νόσου Ιώβ˙ αυτόθι.
περί τής τών πρωτοπλάστων απάτης καί τού όφεως˙ αυτόθι.
περί τού λεγεώνος˙ αυτόθι.
περί τού φυσικού νόμου˙ αυτόθι.
περί τού γραπτού νόμου καί τού πνευματικού˙ αυτόθι.
Λόγος ς'. Εις εμαυτόν μετά τήν επάνοδον. 408.
Πρός δέ:
Περί Ιώβ τού δικαίου. 408.
περί τριών τελώνων. 409.
περί τριών παραλύτων˙ αυτόθι.
περί τών γ'. νεκρών εν Ευαγγελίοις. 410.
Λόγος ζ'. Εις επισκόπους. 411.
Πρός δέ:
Περί Ιωνά καί τού κήτους. 411.
περί Πριάμου, Αχιλλέως, Έκτορος καί τού Τρωϊκού πολέμου˙ αυτόθι.
περί τών γενεθλίων καί ευωχίων Ελλήνων. 412.
περί συνόδων γεράνων καί χηνών. 413
περί κερκώπων τών δολίων, καί περί Πασσάλου καί Ακλήμονος τών ληστών˙ αυτόθι.
Λόγος η'. Περί ευτελείας τού εκτός ανθρώπου. 414.
Πρός δέ:
Περί Αχιλλέως, Αίαντος, Διομήδους, Έκτορος, Ηρακλέους. 415.
Λόγος θ'. Εις εμαυτόν καί τάς κακοπαθείας. 415
Πρός δέ:
Περί Ιώβ καί τών πειρασμών. 415.
Λόγος ι'. Εις εμαυτόν. 416.
Πρός δέ:
Περί τής μελλούσης ανταποδόσεως, καί τών αποκειμένων τοίς αμαρτωλοίς κολάσεως. 416.
Λόγος ια'. Εις τήν εκκλησίαν Αναστασίαν. 416.
Πρός δέ:
Περί Βηθλεέμ. 416.
περί τής αποικίας Ιερουσαλήμ˙ λείπει.
περί τής αιχμαλωσίας κιβωτού˙ λείπει.
περί τής κλοπής τού Ιωσήφ˙ λείπει.
Λόγος ιβ'. Άνευ επιγραφἠς. 417.
Πρός δέ:
Περί τής Αδάμ απάτης˙ λείπει.
περί τών τεταγμένων τοίς ακουσίως φονεύουσι φυγαδευτηρίων πόλεων˙ λείπει.
περί τού μάννα˙ λείπει.
περί τού ει καί Σαούλ εν προφήταις˙ λείπει.
περί Μωϋσέως καί εστηκότων υπό τό όρος. 417.
περί τού εμπυρισμού τών υιών Ααρών˙ αυτόθι.
περί τού οικτρού τέλους Ηλεί καί τών υιών αυτού. 418.
περί Οζά καί τής κιβωτού˙ αυτόθι.
περί τών ερεισμάτων τού ναού. 419.
περί τής κιβωτού καί τής μωρίας τών αλλοφύλων εν ταίς έδραις, καί τής τιμής τής βασάνου˙ αυτόθι.
περί τών αποστόλων καί Ιούδα. 420.
περί Ιεροσολύμων καί Σαμαρείας˙ αυτόθι.
περί τών Μωαβιτών, καί Αμμανιτών, καί Βαλαάκ καί Βαλαάμ καί τής πορνείας τού λαού, καί τών θυγατέρων Λώτ˙ αυτόθι.
περί τών Γαβαωνιτών. 423.
περί τών Λευϊτών˙ αυτόθι.
περί τής Νώε κιβωτού˙ αυτόθι.
περί τού Σοδομιτικού πυρός καί τής τούτου φυγής˙ αυτόθι.
Λόγος ιγ'. Εις επισκόπους. 423.
Πρός δέ:
Περί τής Άρεως Θρασύτητος καί τής τούτου τρώσεως. 424.
περί Λήθης καί τής Ελλήνων τερατολογίας. 425.
Λόγος ιδ'. Περί φύσεως ανθρωπίνης. 426.
Πρός δέ:
Περί τής πικράς βρώσεως καί τιμωρίας. 426.