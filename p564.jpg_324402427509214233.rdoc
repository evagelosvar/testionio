@ΑΙΩΝ ΙΒ΄.- ΕΥΘΥΜΙΟΣ ΖΙΓΑΒΗΝΟΣ κλπ.- ΤΟΜΟΣ 128 - 129 {p 593}
      
ΠΡΟΣΘΗΚΑΙ

ΝΙΚΗΤΑ ΣΕΡΡΩΝΕΙΟΥ
μητροπολίτου Ηρακλείας
Εξήγησις είς τούς λόγους τού αγίου Γρηγορίου Ναζιανζηνού λατινιστί σωζόμενη. 
Είς τόν  λη΄. λόγον. 1176-1212.
Είς τόν  λθ΄. λόγον. 1212-1244.
Είς τόν  μ'. λόγον. 1244-1302.
Είς τόν με'. λόγον, κατ' άλλους μβ΄. 1031.-1410.
Είς τόν μδ΄. ή μγ΄.     1410-1480.

ΝΙΚΗΦΟΡΟΥ ΤΟΥ ΒΟΤΑΝΕΙΑΤΟΥ
( Περί το 1078 Μ.Χ.)
Χρυσόβουλλον επικυρούν τάς συνοδικάς αποφά-σεις, τάς περί τών αθεμίτων γάμων καί τής μνηστείας κτλ. 1481.

ΝΙΚΗΤΑΣ ΣΙΔΗΣ
(Περί τάς αρχάς τού ΙΒ'. αιώνος).
Περί τού Πασχα.     1484.
*Πίναξ είς Νικηφόρων Βρυέννιον λατ. 1488.
*Πίναξ είς Κωνςαντίνου Μανασσήν λατ. 1493.
*Πίναξ τών περιεχομένων έν τώ εκατοστώ εικοστώ εβδόμω τόμω λατινιστί. 1505.
Τέλος τών έν τώ εκατοστώ εικοστώ εβδόμω τόμω.

ΕΥΘΥΜΙΟΣ Ο ΖΙΓΑΒΗΝΟΣ
(1080-1122 Μ.Χ.)

ΕΥΘΥΜΙΟΥ ΤΟΥ ΖΙΓΑΒΗΝΟΥ ΤΑ ΕΥΡΙΣΚΟΜΕΝΑ ΠΑΝΤΑ (α)

ΤΟΜΟΣ 128

ΠΕΡΙΕΧΟΜΕΝΑ
*Περί του Ευθύμιου Ζηγαβηνού προλεγόμενα, λατινιστί. 11.
Υπομνήματα εις τους ψαλμούς. Προοίμιον. 41.
Αρχή της εξηγήσεως. 73-1326.
*Πίναξ των περιεχομένων εν τω εκατοστώ εικοστώ ογδόω τόμω, λατινιστί. 1327.
Τέλος των εν τω εκατοστώ εικοστώ ογδόω τόμω.

ΤΟΜΟΣ 129

ΠΕΡΙΕΧΟΜΕΝΑ 
υπομνήματα είς τά Δ'. Ευαγγέλια. 
*Προοίμιον λατινιστί. 11.
Ερμηνεία είς τό κατά Ματθαίου ευαγγέλιον. 112.
Ερμηνεία είς τό κατά Ευαγγέλιον. 765.
Ερμηνεία είς τό κατά Λουκάν Ευαγγέλιον. 853.
 
---
(α) Είς τόμ. τέσσαρας (128 - 131).
Ανωνύμου μετάφρασις τών δύο έν τώ κατά Λουκάν Ευαγγελίω ωδών Θεοτόκου καί Ζαχαρίου. 1101.
Ερμηνεία είς τό κατά Ιωάννην Ευαγγέλιον. 1106.
*Πίναξ τών περιεχομένων έν τώ εκατοστώ εικοστώ εννάτω τόμω λατινιστί. 1504.
Τέλος τών έν τώ εκατοστώ εικοστώ εννάτω
