@ΑΙΩΝ ΙΒ'-ΑΝΤΩΝΙΟΣ ΜΟΝΑΧΟΣ-ΤΟΜΟΣ 136 {p 518 }

νή καί ορθρινή ,ότε ή τού μακαριωτάτου Χούμνου παραπεσούσα μετά τήν άλωσιν  ούχ ευρίσκετο , 284.                                                                          
Λόγος είς τούς τρείς παίδας Ανανίαν ,Αζαρίαν, καί Μισαήλ. 289.                                                                                          
Λόγος περί υπακοής καί ευπειθείας πρεπούσης πολιτεύματι χριστιανικώ. 301.       
Πρόλογος τών πινδαρικών παρεκβολών, έτι έν διακόνους όντος. 359.                                          
Περί υποκρίσεως ποιητικής. 373. 
Πρός τούς επεγκαλούντας αυτώ μνησικακίαν,είποτε αναμνησθείη κακώσεως γενομένης ποθέν αυτώ. (Απολογία πάνυ αξία καί ωφέλιμος). 408.
Ερμηνεία είς τούς υπό Δαμασκηνού ύμνους τής Πεντηκοστής. 504.
Μονωδία τού σοφώτατου Κυρίου Ευθυμόυ, αναγνωσθείσα επί τού τάφου τού αγιωτάτου θεσσαλονικης Κυρίου Ευσταθίου μετά τίνας ημέρας τής αυτού τελευτής λήγοντος τού ΙΒ'. αιώνος. 756.                                                                                                    

ΑΝΤΩΝΙΟΣ ΜΟΝΑΧΟΣ ο καλούμενος ΜΕΛΙΣΣΑ
(ού τό έτος αμφισβητήσιμον. Τινές δε λέγουσιν ότι ήκμασε περί τό 1110).                            

ΜΕΛΙΣΣΑ
Ήτοι συλλογή διαφόρων θεολογικών κεφαλαίων ώς ή τού Μαξίμου τού Ομολογητού, ής συναζούσης τό γλυκύ μέλι έκ τών ανθέων τού αγρού. Διαιρείται    δέ είς βιβλία δύο .                                                                  
Λόγοι του Α' Βιβλίου ΟΕ' . 
Α'. Περί πίστεως καί ευσεβείας είς Θεόν , καί  ότι παντός αγαθού υπερέχουσιν , οπηνίκα  μάλιστα διά τών έργων λάμπουσιν .  765.     
Β'. Περί τών παραβεβηκότων τήν ευσέβειαν καί  πίστιν , καί είς θεόν ασεβούντων.   773.  
Γ'. Περί αγάπης καί φόβου πρός τόν Θεόν.  781.                                    
Δ'. Περί τών μή φοβουμένων τόν θεόν ,μηδέ αγαπώντων αυτόν . 785.                                 
Ε'. Περί ελπίδος είς θεόν καί ότι χρή είς Θεόν  ελπίζειν , καί μή είς τάς τών ανθρώπων προστασίας. 785. 
ς'. Περί τών πεποιθότων επί πλούτω καί ανθρώποις  καί μή είς θεόν τήν ελπίδα καί πεποίθησίν εχόντων. 789.                                                                                    
Ζ'. Περί αρετής. 792.                                   
Η'. Περί σοφίας καί συνέσεως , καί φρονήσεως.   796.                               
Θ'. Περί αφροσύνης ,άφρονος, καί ανοήτου ,καί απαιδεύτου καί μωρού.797.
Ι'. Περί βουλής, καί ότι χρή μετά βουλής καί σκέψεως πάντα ποιείν. 800.
ΙΑ'. Περί αβουλίας, καί βραδυβουλίας, καί ότι τό απερίσκεπτον βλαβερόν. 804.
ΙΒ'. Περί ανδρείας καί ισχύος. 804.
ΙΓ'. Περί δικαιοσύνης. 804/
ΙΔ'. Περί αγνείας καί σωφροσύνης, και παρθ-νίας, καί γάμου σεμνού. 808.
ΙΕ'. Περί πορνείας καί μοιχείας. 813.
Ις'. Περί αδικίας καί αμαρτίας καί εξαγορεύ-σεως. 816.
ΙΖ'. Περί μετανοίας καί εξομολογήσεως. 824.
ΙΗ'. Περί τών ταχέως μεταβαλλομένων, καί περί μετανοίας. 832.
ΙΘ'. Περί τών είς μετάνοιαν μή επιστρεφόν-των. 833.
Κ'. Περί μελλούσης κρίσεως. 836.
ΚΑ'. Περί αλήθείας καί μαρτυρίας πιστής. 841.
ΚΒ'. Περί ψεύδους καί συκοφαντίας καί ψευδο-μαρτυρίας. 844.
ΚΓ'. Περί αδελφών καί φιλαδελφίας, καί μισα-δελφίας. 845.
ΚΔ'. Περί φίλων καί φιλαδελφίας. 848.
ΚΕ'.Περί φίλων μοχθηρών.
Κς'. Περί αγάπης, καί ειρήνης, καί ομονοίας, καί ειρηνοποιών. 853.
ΚΖ'. Περί ελεημοσύνης καί ευποϊας είς πτω-χούς. 861.                                                                                                          ΚΗ' Περί φειδωλών καί αμεταδότων,                                                                   καί ελεημοσύνην μή ποιούντων.                 873.                                                                  ΚΘ'. Περί ευεργεσίας καί χάριτος καί αχαρι-





