@ΑΙΩΝ Θ΄. - ΦΩΤΙΟΣ ΚΩΝΣΤΑΝΤΙΝΟΥΠΟΛΕΩΣ - ΚΑΝΟΝΙΚΑ - ΤΟΜΟΣ 104 {p 452}

ΤΟΜΟΣ 104

ΠΕΡΙΕΧΟΜΕΝΑ
* Σχόλια εις τήν βιβλιοθήκην Φωτίου. ελληνο-λατινιστί. 356.
* Τί εστιν ο άδης καί πού κείται; 361.
* Ει ορώνται, οι δίκαιοι υπό τών αμαρτωλών. 361.
* Μεταξοσκώληκες καί μέταξα. 370.
* Περί Κροκόπτων ζώων. 373.
* Εί εστιν όρνις ο Φοίνιξ. 382.

ΚΑΝΟΝΙΚΑ
* Προοίμιον εις τό σύνταγμα τών ιερών κανόνων. λατινιστί μετά ελληνικών χωρίων. 431.             
Σύνταγμα κανόνων διηρημένον εις τίτλους ΙΔ΄.
Τίτλος Α΄. Περί θεολογίας, καί ορθοδόξου πίστεως, καί κανόνων, καί χειροτονιών, κεφαλ. λη΄ι 441-560.
Τίτλος Β΄. Περί ποιήσεως εκκλησιών, καί περί ιερών σκευών, καί αναθημάτων, καί κληρικών παρά γνώμην επισκόπων ιστώντων θυσιαστήρια. κεφάλαια Γ΄. 561-588.
Τίτλος Γ΄. Περί ευχών, καί ψαλμωδιών, καί αναγνώσεως, καί αναφοράς, καί φορεσίας, καί υπουργίας αναγνωστών, ψαλτών καί υπηρετών. κεφ. ΚΒ΄. 588-613.
Τίτλος Δ΄. Περί κατηχουμένων καί τού αγίου βαπτίσματος. κεφ.ΙΖ΄. 616-628.
Τίτλος Ε΄. Περί τών καταφρονούντων τών εκκλησιών, καί συνάξεων, καί μνημών, καί περί τών εστιώντων εν εκκλησία, καί περί αγαπών. κεφάλαια Γ΄. 628-632.
Τίτλος ΣΤ΄. Περί καρποφοριών. κεφ.Γ΄. 632-636.
Τίτλος Ζ΄. Περί νηστείας, καί τής τεσσαρακοστής, καί τού Πάσχα, καί τής Πεντηκοστής, καί Κυριακής, καί Σαββάτου, καί περί γονυκλισίας. κεφάλ. Ε΄. 636-648.
Τίτλος Η΄. Περί παροικιών, καί πώς διάγουσιν επίσκοποι καί κληρικοί, καί περί αποδημίας αυτών, καί περί τών κατ'έτος συνόδων, καί ξενοδοχίας, καί διδασκαλίας, καί συστατικής καί ειρηνικής επιστολής, καί ποία πράττουσιν ιδιωτικά ή δημόσια πράγματα, καί πώς οί κληρικοί αλλήλους τιμώσι. κεφ.ΙΗ΄. 649-697.
Τίτλος Θ΄. Περί αμαρτημάτων καί δικών επισκόπων, καί κληρικών, καί αφορισμού, καί καθαιρέσεως, καί ποία αμαρτήματα ή χειροθεσία λύει. κεφάλ.ΛΘ΄. 697-816.
Τίτλος Ι΄. Περί διοικήσεως εκκλησιαστικών πραγμάτων καί περί τών ιδικών τού επισκόπου. κεφ.Η΄. 816-833.
Τίτλος ΙΑ΄. Περί μοναστηρίων καί μοναχών, κεφάλ. ΙΣΤ΄. 836-864.
Τίτλος ΙΒ΄. Περί αιρετικών, Ιουδαίων, καί Ελλήνων. κεφάλ. ΙΗ΄. 865-889.
Τίτλος ΙΓ΄. Περί λαϊκών. κεφ. Μ΄. 889-969.
Τίτλος ΙΔ΄. Περί κοινών πάντων ανθρώπων. κεφάλ. Ζ΄. 969-976.
               
ΝΟΜΟΚΑΝΩΝ
Μετά υπομνημάτων Θεοδώρου Βαλσαμώνος, τού μετά ταύτα πατριάρχου Αντιοχειας γενομένου.
Εξήγησις τών ιερών καί θείων κανόνων εις τίτλους ΙΔ΄.
* Προοίμιον τού Θεοδώρου Βαλσαμώντος πόθεν εκινήθη εις τήν εξήγησιν. 976.                 -
Τίτλος Α΄. Περί θεολογίας, καί ορθοδόξου πίστεως, καί κανόνων, καί χειροτονιών. κεφ. Α΄. - ΔΗ΄. 980-1025.
Τίτλος Β΄. Περιέχει όσα καί ο Β΄. τίτλος τού συντάγματος τών κανόνων. κεφάλαιον Α΄. - Γ΄. 1028-1049.
